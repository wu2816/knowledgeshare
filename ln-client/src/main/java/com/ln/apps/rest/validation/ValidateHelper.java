/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.validation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class ValidateHelper {

    private static final Logger logger = LoggerFactory.getLogger(ValidateHelper.class);


    public static String truncateValue(Class clazz, String fieldName, String value) {
        if (StringUtils.isEmpty(value) || StringUtils.isEmpty(fieldName)) {
            return null;
        }
        try {
            int size = clazz.getDeclaredField(fieldName).getAnnotation(Size.class).max();
            int inLength = value.length();
            if (inLength > size) {
                logger.warn("The length of " + fieldName + " (" + value + ") exceeds " + size + ". It is truncated to " + size + " characters.");
                value = value.substring(0, size);
            }
        } catch (NoSuchFieldException ex) {

            logger.error("The filed name " + fieldName + " is not a valid in Entity object. Error message is: " + ex.getMessage(), ex);

        } catch (SecurityException ex) {

            logger.error("The filed name " + fieldName + " is not accessible in Entity object. Error message is: " + ex.getMessage(), ex);

        }
        return value;
    }


    public static BigDecimal convertBigDecimalInRange(Class clazz, String fieldName, BigDecimal value, BigDecimal defaultValue) {

        if (value == null || StringUtils.isEmpty(fieldName)) {
            value = defaultValue;
        } else {
            BigDecimal max = null;
            BigDecimal min = null;
            try {
                max = new BigDecimal(clazz.getDeclaredField(fieldName).getAnnotation(DecimalMax.class).value());
                min = new BigDecimal(clazz.getDeclaredField(fieldName).getAnnotation(DecimalMin.class).value());
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }

            if (value.compareTo(max) > 0 || value.compareTo(min) < 0) {
                logger.warn("The length of " + fieldName + " (" + value + ") is not in the range [" + min + "," + max + "]. It is replaced by default value: " + defaultValue + ".");
                value = defaultValue;
            }
        }
        return value;
    }

    public static String replaceDefaultValue(Class clazz, String fieldName, String value, String defaultValue) {

        if (StringUtils.isEmpty(value) || StringUtils.isEmpty(fieldName)) {
            return defaultValue;
        }

        try {

            String regexp = clazz.getDeclaredField(fieldName).getAnnotation(Pattern.class).regexp();

            if (value.matches(regexp)) {
                return value;
            } else {
                logger.warn("The format of " + fieldName + " value (" + value + ") does not match with regexp "+ regexp + ". It is replaced by default value: " + defaultValue + ".");
                return defaultValue;
            }

        } catch (NoSuchFieldException ex) {

            logger.error("The filed name " + fieldName + " is not a valid in Entity object. Error message is: " + ex.getMessage(), ex);

        } catch (SecurityException ex) {

            logger.error("The filed name " + fieldName + " is not accessible in Entity object. Error message is: " + ex.getMessage(), ex);

        }
        return value;
    }
}
