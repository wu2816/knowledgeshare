/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.services;

import com.ln.apps.rest.model.Entity;
import com.ln.apps.rest.model.Response;
import com.ln.apps.rest.utils.AppConstants;
import com.ln.apps.rest.utils.ParserUtil;
import com.ln.apps.rest.validation.EntityValidator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;


@Service
@Component
@Transactional
public class EntityServiceImpl implements EntityService {

    private static final Logger logger = LoggerFactory.getLogger(EntityServiceImpl.class);

    @Autowired
    private EntityValidator entityValidator;

    @Override
    public Response greeting(String firstName, String lastName) {

        return getResponse(firstName, lastName);
    }

    @Override
    public Response process(List<Entity> entityList) {

        String uuid = MDC.get(AppConstants.LN_REQUEST_ID);
        if(StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString();
        }

        List<Entity> validEntityList;
        List<Entity> inValidEntityList = new CopyOnWriteArrayList<>();
        List<Entity> invalidErrorOnlyEntityList = new CopyOnWriteArrayList<>();
        List<Set<ConstraintViolation<Entity>>> violationSetList = new CopyOnWriteArrayList<>();

        List<Entity> inValidEntityResponseList = new CopyOnWriteArrayList<>();

        validEntityList = entityValidator.checkGenericList(entityList, inValidEntityList, violationSetList);

        int errorCount = 0;
        if(inValidEntityList != null && inValidEntityList.size() > 0) {

            errorCount = getEntityResponseList(inValidEntityResponseList, validEntityList, inValidEntityList, violationSetList, uuid, invalidErrorOnlyEntityList);
            logger.info("Call getEntityResponseList, total number of records with error: " + errorCount);
        }

        if(entityList == null || entityList.size() ==0) {
            return getResponse("Default_FN", "Default_LN");
        }
        Response response = getResponse(entityList.get(0).getFirstName(), entityList.get(0).getLastName());
        response.setData(inValidEntityList);
        return response;
    }

    private Response getResponse(String firstName, String lastName) {
        String uuid = MDC.get(AppConstants.LN_REQUEST_ID);
        if (StringUtils.isBlank(uuid)) {
            uuid = UUID.randomUUID().toString();
        }

        Response response = new Response();
        response.setId(uuid);

        response.setMessageDetail("Hello " + firstName + " " + lastName + ". Greeting from Ln Client.");
        return response;
    }

    private int getEntityResponseList(List<Entity> invalidResponseEntityList,List<Entity> validEntityList, List<Entity> inValidEntityList,
                                      List<Set<ConstraintViolation<Entity>>> violationSetList, String uuid, List<Entity> invalidErrorOnlyEntityList) {

        int i, size = inValidEntityList.size();
        Entity entity, truncatedValueEntity;
        StringBuilder errorMessageSb;
        String errorMessage;
        int errorCount = 0;

        for( i = 0; i < size; i++) {
            entity = inValidEntityList.get(i);
            Set<ConstraintViolation<Entity>> violationSet = violationSetList.get(i);

            errorMessageSb = new StringBuilder("");
            Map<String, Integer> fieldWithErrorMap = ParserUtil.getFieldWithErrorMap(violationSet, errorMessageSb);
            errorMessage = StringUtils.chop(errorMessageSb.toString());

            entity.setErrorMessage(errorMessage);
            if(errorMessage.startsWith(AppConstants.ERROR_FLAG)) {  //reject
                entity.setErrorCode(AppConstants.INVALID_PARAM_CODE);
                invalidResponseEntityList.add(entity);
                invalidErrorOnlyEntityList.add(entity);
                errorCount++;
                continue;
            }

            //Truncate and insert for WARNINGs.
            entity.setErrorCode(AppConstants.INVALID_PARAM_WARNING_CODE);

            invalidResponseEntityList.add(entity); //add the record with warning to response object.

            if(fieldWithErrorMap == null || fieldWithErrorMap.size() ==0 ) {
                logger.warn("For Entity:" + entity + ", some of the values of the fields were truncated. The validation error message is: " + errorMessage);
                truncatedValueEntity = new Entity(entity.getFirstName(),
                        entity.getLastName(),
                        entity.getAge());
                validEntityList.add(truncatedValueEntity);
            }
        }

        return errorCount;
    }
}
