/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.client.controller;


import com.ln.apps.rest.model.Entity;
import com.ln.apps.rest.model.Response;
import com.ln.apps.rest.model.ServiceStatus;
import com.ln.apps.rest.services.EntityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RefreshScope
@RequestMapping("/v1")
public class LnClientApiGateway {

    private static final Logger logger = LoggerFactory.getLogger(LnClientApiGateway.class);

    @Autowired
    private final EntityService entityService;

    @Autowired
    public LnClientApiGateway(EntityService entityService) {
        this.entityService = entityService;
    }

    @PostMapping(value = "/process", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> process(@RequestBody(required = false) List<Entity> entityList) {

        Response response = entityService.process(entityList);

        if (response != null) {
            return ResponseEntity.ok(response);
        }

        response = new Response();
        response.setId(UUID.randomUUID().toString());
        response.setStatus(ServiceStatus.SUCCESS);
        response.setData(String.format("Thanks: [param_value=%s]", entityList));
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value = "/greeting", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public Response greeting(@RequestParam(name = "firstName") String firstName, @RequestParam(name = "lastName") String lastName) {
        return entityService.greeting(firstName, lastName);
    }

}
