/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.model;

public enum ServiceStatus {

    /**
     * Request was successful.
     */
    SUCCESS(200),

    /**
     * Request was successful.
     */
    BAD_REQUEST(400),

    /**
     * Request does not have sufficient permissions
     */
    FORBIDDEN(403),

    /**
     * The specified resource does not exist.
     */
    NOT_FOUND(404),

    /**
     * The resource doesn't support the specified HTTP verb
     */
    METHOD_NOT_ALLOWED(405),

    /**
     * The specified account already exists.
     */
    CONFLICT(405),

    /**
     * The server encountered an internal error.
     */
    INTERNAL_SERVER_ERROR (500),

    /**
     * The server is currently unable to receive requests
     */
    SERVICE_UNAVAILABLE (503),

    /**
     * Request failed.
     */
    FAILED(500),

    /**
     * Request failed.
     */
    RULE_FAILURE(500);

    /**
     * Response code.
     */
    private final int status;

    /**
     * Set the response code in the constructor.
     *
     * @param status
     */
    ServiceStatus(final int status) {
        this.status = status;
    }

    /**
     * Return the response code.
     *
     * @return status
     */
    public int getCode() {
        return status;
    }
}
