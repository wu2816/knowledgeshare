/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ln.apps.rest.validation.DataChecks;
import com.ln.apps.rest.validation.ValidateHelper;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public class Entity extends LnMessage {

    public Entity() {}

    public Entity(String firstName, String lastName, BigDecimal age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    @NotNull(message = "{entity.FirstName.null.msg}", groups = DataChecks.class)
    @Size(min = 1, max = 30, message = "{entity.FirstName.size.msg}", groups = DataChecks.class)
    @Pattern(regexp = "[\\w .'-]{1,30}", message = "{entity.FirstName.pattern.msg}", groups = DataChecks.class)
    @SerializedName("FirstName")
    @Expose
    private String firstName;

    @NotNull(message = "{entity.LastName.null.msg}", groups = DataChecks.class)
    @Size(min = 1, max = 30, message = "{entity.LastName.size.msg}", groups = DataChecks.class)
    @Pattern(regexp = "[\\w .'-]{1,30}", message = "{entity.LastName.pattern.msg}", groups = DataChecks.class)
    @SerializedName("LastName")
    @Expose
    private String lastName;

    @DecimalMax(value = "150", message = "{entity.Age.DecimalMax.msg}", groups = DataChecks.class)
    @DecimalMin(value = "0", message = "{entity.Age.DecimalMin.msg}", groups = DataChecks.class)
    @SerializedName("Age")
    @Expose
    private BigDecimal age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = ValidateHelper.truncateValue(getClass(), "firstName", firstName);
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = ValidateHelper.truncateValue(getClass(), "lastName", lastName);
    }

    public BigDecimal getAge() {
        return age;
    }

    public void setAge(BigDecimal age) {
        this.age = ValidateHelper.convertBigDecimalInRange(getClass(), "age", age, new BigDecimal(0));
    }
}
