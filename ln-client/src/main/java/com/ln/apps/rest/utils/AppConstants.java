/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.utils;

public class AppConstants {

    public static final String LN_REQUEST_ID = "LN_REQUEST_ID";
    public static final String UTC_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";
    public static final String PIPE = "|";
    public static final String ERROR_FLAG = "ERROR: ";
    public static final String WARNING_FLAG = "WARNING: ";
    public static final int SIZE_MAX_NOT_DEFINED_VALUE = -1;
    public static final String INVALID_PARAM_CODE = "-50";

    public static final String INVALID_PARAM_WARNING_CODE = "-782";


}
