/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class ParserUtil {

    private static final Logger logger = LoggerFactory.getLogger(ParserUtil.class);

    public static Date getDate(String dateString) {
        Date retDate = null;
        try {
            retDate = dateFromStringFormat(dateString, DateUtil.LN_UTC_DATE_TIME_FORMAT);
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
            try {
                retDate = dateFromStringFormat("1970-01-01T00:00:00Z", DateUtil.LN_UTC_DATE_TIME_FORMAT);
            } catch (Exception e2) {
                logger.debug(e2.getMessage(), e2);
            }

        }

        return retDate;
    }

    public static Date dateFromStringFormat(final String date, final String format) throws NullPointerException, ParseException {

        if (date == null) {
            throw  new NullPointerException("DateUtil: DateFromStringFormat: date == null");
        }

        if (format == null) {
            throw  new NullPointerException("DateUtil: DateFromStringFormat: format == null");
        }

        DateFormat dateFormat = new SimpleDateFormat(format, Locale.ENGLISH);

        return dateFormat.parse(date);
    }

    public static Boolean getBoolean(String dataString) {
        return "true".equalsIgnoreCase(dataString);
    }

    public static BigInteger getBigInteger(String dataString) {
        BigInteger retValue;

        try{
            retValue = new BigInteger(dataString);
        } catch (NumberFormatException e) {
            logger.debug(e.getMessage(), e);
            retValue = new BigInteger("-1");
        }
        return retValue;
    }


    public static BigDecimal getBigDecimal(String dataString) {
        BigDecimal retValue;

        try{
            retValue = new BigDecimal(dataString);
        } catch (NumberFormatException e) {
            logger.debug(e.getMessage(), e);
            retValue = new BigDecimal(-1);
        }
        return retValue;
    }

    public static <T> String getReasonForViolations(Set<ConstraintViolation<T>> set) {
        if (set == null || set.size() == 0) {
            return "";
        } else {
            String reasonStr = set.parallelStream()
                    .map(e -> e.getMessage() + AppConstants.PIPE)
                    .collect(Collectors.joining());
            logger.debug(reasonStr);
            return StringUtils.chop(reasonStr);
        }
    }

    public static <T> Map<String, Integer> getFieldWithErrorMap(Set<ConstraintViolation<T>> dataViolations, StringBuilder messageSb) {

        Map<String, Integer> maxFieldLengthMap = new HashMap<>();
        String fieldName;
        String message;

        for(ConstraintViolation violation : dataViolations) {

            message = violation.getMessage();
            messageSb.append(message + AppConstants.PIPE); //validation error messages.

            fieldName = violation.getPropertyPath().toString();

            if(message.contains(AppConstants.ERROR_FLAG)) {
                maxFieldLengthMap.put(fieldName, AppConstants.SIZE_MAX_NOT_DEFINED_VALUE); //not for insertion.
            }

        }

        return maxFieldLengthMap;
    }

    public static byte[] serialize(Object obj) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(obj);
            return out.toByteArray();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    public static Object deserialize(byte[] data)  {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        try {
            ObjectInputStream is = new ObjectInputStream(in);
            return is.readObject();
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage(),e);
        }
        return null;
    }

}

