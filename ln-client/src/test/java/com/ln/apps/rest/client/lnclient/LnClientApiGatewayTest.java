/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.client.lnclient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.ln.apps.rest.client.controller.LnClientApiGateway;
import com.ln.apps.rest.model.Response;
import com.ln.apps.rest.model.ServiceStatus;
import com.ln.apps.rest.model.Entity;
import com.ln.apps.rest.utils.AppConstants;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class LnClientApiGatewayTest extends BaseRestTestSupport {

    @InjectMocks
    private LnClientApiGateway lnClientApiGateway;

    @Override
    public MockMvc getMockMvc() {
        return MockMvcBuilders
                .standaloneSetup(lnClientApiGateway)
                .apply(documentationConfiguration(this.restDocumentation)).alwaysDo(this.document)
                .build();
    }

    @Test
    public void greeting() throws Exception {

        Response response = new Response();
        response.setStatus(ServiceStatus.SUCCESS);
        response.setId("e362b14c-cb39-4830-a242-48d081d45245");

        Entity entityResponse = new Entity();
        entityResponse.setFirstName("Joe");
        entityResponse.setLastName("Doe");
        response.setData(entityResponse);

        Mockito.when(entityService.greeting(Mockito.anyString(), Mockito.anyString())).thenReturn(response);

        this.mockMvc.perform(get("/v1/greeting")
                .param("firstName", "Joe")
                .param("lastName", "Doe")
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.data.errorCode", is("-50")))
                .andDo(document("v1/greeting",
                        requestParameters(
                                parameterWithName("firstName").description("First Name"),
								parameterWithName("lastName").description("Last Name")
                        )
                ));
    }

    @Test
    public void process() throws Exception {

        Response response = new Response();
        response.setStatus(ServiceStatus.SUCCESS);
        response.setId("e362b14c-cb39-4830-a242-48d081d45245");
        response.setData("e362b14c-cb39-4830-a242-48d081d45245");

        List<Entity> entityList = new ArrayList<>();
        Entity entity;
        for (int i = 0; i < 7; i++) {
            entity = new Entity("Joe" + i, "Doe" + i, new BigDecimal(i));
            entityList.add(entity);
        }


        Gson gson = new GsonBuilder().setDateFormat(AppConstants.UTC_DATE_TIME_FORMAT).create();

        Mockito.when(entityService.process(Mockito.anyList())).thenReturn(response);

        this.mockMvc.perform(post("/v1/process")
                .content(gson.toJson(entityList))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is("e362b14c-cb39-4830-a242-48d081d45245")))
                .andDo(document("process",
                        requestParameters()
                ))
                .andExpect(status().isOk());
    }


}
