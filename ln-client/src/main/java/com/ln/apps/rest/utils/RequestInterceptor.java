/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.UUID;

public class RequestInterceptor extends HandlerInterceptorAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RequestInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        String requestId = UUID.randomUUID().toString();
        MDC.put(AppConstants.LN_REQUEST_ID, requestId);

        if (LOGGER.isInfoEnabled()) {
            long startTime = System.currentTimeMillis();

            String message = "Request :" + request.getRequestURL().toString()
                    + (request.getQueryString() != null ? "?" + request.getQueryString() : "")  // append query string if any
                    + " received";
            LOGGER.info(message);
            request.setAttribute("startTime", startTime);

            //logging request parameters
            LOGGER.info("Paramters list");
            request.getParameterMap().forEach((key, value) ->
                    LOGGER.info(key + ":" + String.join(",", value))
            );

            //logging request header
            LOGGER.info("Headers list");
            Enumeration headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String headerName = (String) headerNames.nextElement();
                String headerValue = request.getHeader(headerName);
                LOGGER.info(headerName + ":" + headerValue);
            }
        }

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex) throws Exception {

        if (LOGGER.isInfoEnabled()) {
            long startTime = (Long) request.getAttribute("startTime");

            String message = " Completed Request: " + request.getRequestURL().toString()
                    + (request.getQueryString() != null ? "?" + request.getQueryString() : "")  // append query string if any
                    + " in " + (System.currentTimeMillis() - startTime + "ms");
            LOGGER.info(message);
        }
    }

}