/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.utils;

import com.ln.apps.rest.model.Response;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;

public class RestClient {

    private static final Logger logger = LoggerFactory.getLogger(RestClient.class);

    public static void main(String[] args) {

        RestClient restClient = new RestClient();
        restClient.test_http();
        restClient.test_https();

    }

    public void test_http() {
        String url = "http://localhost:8080/v1/greeting?lastName=Wu&firstName=Richard";
        String username = "Test";
        String password = "Test";

        invokeRestService(null, url, username, password);
    }

    public void test_https() {
        String url = "https://localhost:8443/v1/greeting?lastName=Wu&firstName=Richard";
        String username = "Test";
        String password = "Test";

        invokeRestService(null, url, username, password);
    }

    public void invokeRestService(String host, String url, String username, String password) {

        SSLContext sslContext = createSSLContext();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        HttpHeaders requestHeaders = createHeaders(username, password);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
        ResponseEntity<Response> responseData = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Response.class);

        Response response = responseData.getBody();
        logger.debug(response.toString());
        logger.debug(responseData.getStatusCode() + " ---- response");
    }

    private HttpHeaders createHeaders(String username, String password){
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.encodeBase64(
                    auth.getBytes(Charset.forName("US-ASCII")) );
            String authHeader = "Basic " + new String( encodedAuth );
            set( "Authorization", authHeader );
        }};
    }

    private SSLContext createSSLContext() {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
        SSLContext sslContext = null;
        try {
            sslContext = org.apache.http.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
        } catch (KeyStoreException e) {
            logger.error(e.getMessage(), e);
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage(), e);
        } catch (KeyManagementException e) {
            logger.error(e.getMessage(), e);
        }
        return sslContext;
    }

    public <T> Response invokeRestPostService(String host, String url, String username, String password, List<T> inputEntityList) {

        SSLContext sslContext = createSSLContext();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        HttpHeaders requestHeaders = createHeaders(username, password);
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        // Note the body object as first parameter!
        HttpEntity<?> httpEntity = new HttpEntity<Object>(inputEntityList, requestHeaders);

        ResponseEntity<Response> responseData = restTemplate.exchange(url, HttpMethod.POST, httpEntity, Response.class);

        Response response = responseData.getBody();
        logger.debug(response.toString());
        logger.debug(responseData.getStatusCode() + "");

        return response;
    }

 }