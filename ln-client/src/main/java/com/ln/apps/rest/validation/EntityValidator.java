/**
 * Copyright 2017 https://twitter.com/richard8080
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.ln.apps.rest.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class EntityValidator {

    private static final Logger logger = LoggerFactory.getLogger(EntityValidator.class);

    private Validator intakeValidator = Validation.buildDefaultValidatorFactory().getValidator();

    public <T> List<T> checkGenericList(List<T> dataList, List<T> invalidDataList, List<Set<ConstraintViolation<T>>> violationSetList) {
        Set<ConstraintViolation<T>> dataViolations;

        List<T> validDataList = new ArrayList<>();

        for (T data : dataList) {

            dataViolations = intakeValidator.validate(data, DataChecks.class);

            if (dataViolations == null || dataViolations.size() == 0) {
                validDataList.add(data);
            } else {
                invalidDataList.add(data);
                violationSetList.add(dataViolations);
            }

        }
        return validDataList;
    }

}
